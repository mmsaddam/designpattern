//
//  LibraryAPI.m
//  BlueLibrary
//
//  Created by Md Islam on 9/1/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

#import "LibraryAPI.h"

@interface LibraryAPI (){
    PersistencyManager *persistencyManager;
    HTTPClient *httpClient;
    BOOL isOnline;

}
@end

@implementation LibraryAPI
+ (LibraryAPI*)sharedInstance
{
    // 1
    static LibraryAPI *_sharedInstance = nil;
    
    // 2
    static dispatch_once_t oncePredicate;
    
    // 3
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[LibraryAPI alloc] init];
    });
    return _sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        persistencyManager = [[PersistencyManager alloc] init];
        httpClient = [[HTTPClient alloc] init];
        isOnline = NO;
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(downloadImage:) name:@"DownloadImageNotification" object:nil];
    }
    return self;
}

-(void)downloadImage:(NSNotification *)notification{
    UIImageView *imageView = notification.userInfo[@"imageView"];
    NSString *converUrl = notification.userInfo[@"coverUrl"];
    imageView.image = [persistencyManager getImage:converUrl];
    
    if (imageView.image == nil) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *img = [httpClient downloadImage:converUrl];
            dispatch_sync(dispatch_get_main_queue(), ^{
                imageView.image = img;
                [persistencyManager saveImage:img filename:converUrl];
            });
        });
    }
}

- (void)saveAlbums{
    [persistencyManager saveAlbums];
}

- (NSArray*)getAlbums
{
    return [persistencyManager getAlbums];
}

- (void)addAlbum:(Album*)album atIndex:(int)index
{
    [persistencyManager addAlbum:album atIndex:index];
    if (isOnline)
    {
        [httpClient postRequest:@"/api/addAlbum" body:[album description]];
    }
}

- (void)deleteAlbumAtIndex:(int)index
{
    [persistencyManager deleteAlbumAtIndex:index];
    if (isOnline)
    {
        [httpClient postRequest:@"/api/deleteAlbum" body:[@(index) description]];
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
