//
//  LibraryAPI.h
//  BlueLibrary
//
//  Created by Md Islam on 9/1/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PersistencyManager.h"
#import "HTTPClient.h"

@interface LibraryAPI : NSObject

+ (LibraryAPI*)sharedInstance;

- (NSArray*)getAlbums;
- (void)addAlbum:(Album*)album atIndex:(int)index;
- (void)deleteAlbumAtIndex:(int)index;
- (void)saveAlbums;

@end
