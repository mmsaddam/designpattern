//
//  HorizontalScroller.m
//  BlueLibrary
//
//  Created by Muzahidul Islam on 9/2/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

#import "HorizontalScroller.h"

#define VIEW_PADDING 10
#define VIEW_DIMENSIONS 100
#define VIEWS_OFFSET 100

@interface HorizontalScroller() <UIScrollViewDelegate>{
    UIScrollView *scroller;
}

@end
@implementation HorizontalScroller

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        scroller = [[UIScrollView alloc]initWithFrame:self.bounds];
        scroller.delegate = self;
        [self addSubview:scroller];
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollerTapped:)];
        [scroller addGestureRecognizer:tapRecognizer];
    }
    return self;
}

-(void)scrollerTapped:(UITapGestureRecognizer *)gesture{
    
    CGPoint location = [gesture locationInView:gesture.view];
    
    for (int index = 0; index<[self.delegate numberOfViewForHorizontalScroller:self]; index++) {
        
        UIView *view = [scroller.subviews objectAtIndex:index];
        
        if (CGRectContainsPoint(view.frame, location)) {
            
            [self.delegate horizontalScroller:self clickedViewAtIndex:index];
            [scroller setContentOffset:CGPointMake(view.frame.origin.x - self.frame.size.width/2 + view.frame.size.width/2, 0) animated:YES];
            break;
        }
    }
    
}
-(void)reload{
    
    // if there is no delegate nothing to load
    if (self.delegate == nil) {
        return;
    }
    
    // remove subview from the scroller
    
    [scroller.subviews enumerateObjectsUsingBlock:^(__kindof UIView * __nonnull obj, NSUInteger idx, BOOL * __nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    // starting x postion of view under the scroller
    CGFloat xValue = VIEWS_OFFSET;
    for (int i = 0; i<[self.delegate numberOfViewForHorizontalScroller:self]; i++) {
        xValue = xValue + VIEW_PADDING;
        UIView *view = [self.delegate horizontalScroller:self viewAtIndex:i];
        view.frame = CGRectMake(xValue, VIEW_PADDING, VIEW_DIMENSIONS, VIEW_DIMENSIONS);
        [scroller addSubview:view];
        xValue = xValue + VIEW_DIMENSIONS + VIEW_PADDING;
    }
    
    [scroller setContentSize:CGSizeMake(xValue + VIEWS_OFFSET, self.frame.size.height)];
    
    if ([self.delegate respondsToSelector:@selector(intialViewIndexForHorizontalScroller:)]) {
        
        NSUInteger initialViewIndex = [self.delegate intialViewIndexForHorizontalScroller:self];
        [scroller setContentOffset:CGPointMake((initialViewIndex*VIEWS_OFFSET)+(2*VIEW_PADDING), VIEW_PADDING)];
        
    }
    
   
}

// when view added to super view then this method called
- (void)didMoveToSuperview{
    [self reload];
}

// make current view center in the scroller
-(void)centerCurrentView{
    
    int xFinal = scroller.contentOffset.x + (VIEWS_OFFSET/2) + VIEW_PADDING;
    int viewIndex = xFinal / (VIEW_DIMENSIONS+(2*VIEW_PADDING));
    xFinal = viewIndex * (VIEW_DIMENSIONS+(2*VIEW_PADDING));
    [scroller setContentOffset:CGPointMake(xFinal,0) animated:YES];
    [self.delegate horizontalScroller:self clickedViewAtIndex:viewIndex];
}

#pragma mark - UIScrollView Delegate


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self centerCurrentView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self centerCurrentView];
}










@end
