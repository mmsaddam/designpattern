//
//  Album+TableRepresentation.h
//  BlueLibrary
//
//  Created by Md Islam on 9/1/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

#import "Album.h"

@interface Album (TableRepresentation)
- (NSDictionary*)tr_tableRepresentation;
@end
