//
//  Album.h
//  BlueLibrary
//
//  Created by Md Islam on 8/31/15.
//  Copyright © 2015 iMuzahid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Album : NSObject<NSCopying>
@property (nonatomic, copy, readonly) NSString *title, *artist, *genre, *coverUrl, *year;

- (id)initWithTitle:(NSString*)title artist:(NSString*)artist coverUrl:(NSString*)coverUrl year:(NSString*)year;
@end
