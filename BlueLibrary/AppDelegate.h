//
//  AppDelegate.h
//  BlueLibrary
//
//  Created by iMuzahid on 31/7/13.
//  Copyright (c) 2013 iMuzahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
