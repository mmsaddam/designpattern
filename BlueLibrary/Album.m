//
//  Album.m
//  BlueLibrary
//
//  Created by Md Islam on 8/31/15.
//  Copyright © 2015 iMuzahid. All rights reserved.
//

#import "Album.h"

@interface Album ()



@end

@implementation Album

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.artist forKey:@"artist"];
    [aCoder encodeObject:self.coverUrl forKey:@"coverUrl"];
    [aCoder encodeObject:self.year forKey:@"year"];
    [aCoder encodeObject:self.genre forKey:@"genre"];
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        _title=[aDecoder decodeObjectForKey:@"title"];
        _artist = [aDecoder decodeObjectForKey:@"artist"];
        _coverUrl = [aDecoder decodeObjectForKey:@"coverUrl"];
        _year = [aDecoder decodeObjectForKey:@"year"];
        _genre = [aDecoder decodeObjectForKey:@"genre"];
    }
    return self;
}

- (id)initWithTitle:(NSString*)title artist:(NSString*)artist coverUrl:(NSString*)coverUrl year:(NSString*)year{
    
    self = [super init];
    if (self)
    {
        _title = title;
        _artist = artist;
        _coverUrl = coverUrl;
        _year = year;
        _genre = @"Pop";
    }
    return self;
}

@end
