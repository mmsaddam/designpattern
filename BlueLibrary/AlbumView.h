//
//  AlbumView.h
//  BlueLibrary
//
//  Created by Md Islam on 8/31/15.
//  Copyright © 2015 iMuzahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumView : UIView
- (id)initWithFrame:(CGRect)frame albumCover:(NSString*)albumCover;

@end
