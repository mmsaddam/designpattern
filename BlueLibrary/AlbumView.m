//
//  AlbumView.m
//  BlueLibrary
//
//  Created by Md Islam on 8/31/15.
//  Copyright © 2015 iMuzahid. All rights reserved.
//

#import "AlbumView.h"

@implementation AlbumView
{
    UIImageView *coverImage;
    UIActivityIndicatorView *indicator;
}

- (id)initWithFrame:(CGRect)frame albumCover:(NSString*)albumCover
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        self.backgroundColor = [UIColor blackColor];
        // the coverImage has a 5 pixels margin from its frame
        coverImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, frame.size.width-10, frame.size.height-10)];
        [self addSubview:coverImage];
        
        indicator = [[UIActivityIndicatorView alloc] init];
        indicator.center = self.center;
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [indicator startAnimating];
        [self addSubview:indicator];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"DownloadImageNotification" object:self userInfo:@{@"imageView":coverImage, @"coverUrl":albumCover }];
        [coverImage addObserver:self forKeyPath:@"image" options:0 context:nil];
    }
    return self;
}

-(void)observeValueForKeyPath:(nullable NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary *)change context:(nullable void *)context{
    if ([keyPath isEqualToString:@"image"]) {
        [indicator removeFromSuperview];
    }
}
-(void)dealloc{
    [coverImage removeObserver:self forKeyPath:@"image"];
}

@end