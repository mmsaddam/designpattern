//
//  HorizontalScroller.h
//  BlueLibrary
//
//  Created by Muzahidul Islam on 9/2/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HorizontalScrollerDelegate;

@interface HorizontalScroller : UIView

@property (nonatomic, weak) id<HorizontalScrollerDelegate> delegate;

-(void)reload;
@end

@protocol HorizontalScrollerDelegate <NSObject>

@required
// ask the delegate how many views he wants to present inside the horizontal scroller
-(NSUInteger)numberOfViewForHorizontalScroller:(HorizontalScroller *)scroller;

// ask the delegate to return the view that should appear at <index>
-(UIView *)horizontalScroller:(HorizontalScroller *)scroller viewAtIndex:(NSUInteger)index;

// inform the delegate what the view at <index> has been clicked
- (void)horizontalScroller:(HorizontalScroller*)scroller clickedViewAtIndex:(int)index;

@optional
// ask the delegate for the index of the initial view to display. this method is optional
// and defaults to 0 if it's not implemented by the delegate
-(NSUInteger)intialViewIndexForHorizontalScroller:(HorizontalScroller *)scroller;
@end